from django.contrib import admin
from watchlists.models import WatchlistItem

# Register your models here.

class WatchlistItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(WatchlistItem, WatchlistItemAdmin)