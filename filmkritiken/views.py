from django.db import IntegrityError
from django.http import Http404
from django.shortcuts import redirect, render
from django.core.paginator import Paginator
from filmkritiken.models import Film, Review
from watchlists.models import WatchlistItem
from filmkritiken.forms import FilmForm, ReviewForm
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods


# Create your views here.
def films_list(request):
    query = request.GET.get("q")
    if not query:
        query = ""
    films = Film.objects.filter(title__icontains=query)
    paginator = Paginator(films, 9)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        "page_obj": page_obj,
        'total_films': len(films),
    }

    return render(request, "filmkritiken/list.html", context)


@login_required
def new_film(request):
    if request.method == "POST":
        form = FilmForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("films_list_url")
    else:
        form = FilmForm()
    context = {
        "form": form
    }
    return render(request, "filmkritiken/new.html", context)


@login_required
def edit_film(request, pk):
    instance = Film.objects.get(pk=pk)
    if request.method == "POST":
        form = FilmForm(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            return redirect("films_list_url")
    else:
        form = FilmForm(instance=instance)
    context = {
        "form": form
    }
    return render(request, "filmkritiken/edit.html", context)


def detail_film(request, pk):
    film = Film.objects.get(pk=pk)
    try:
        on_watchlist = request.user.watchlist_item.get(film=film)
    except:
        on_watchlist = None
    context = {
        "film": film,
        "on_watchlist": on_watchlist,
    }
    return render(request, "filmkritiken/detail.html", context)


@login_required
def delete_film(request, pk):
    try:
        film = Film.objects.get(pk=pk)
    except Film.DoesNotExist:
        raise Http404("Does not exist")
    if request.method == "POST":
        film.delete()
        return redirect("films_list_url")
    return render(request, "filmkritiken/delete.html")


@login_required
def create_review(request, pk):
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.reviewer = request.user
            instance.film_id = pk
            instance.save()
            return redirect("detail_film_url", pk=pk)

    else:
        form = ReviewForm()
    context = {
        "form": form
    }
    return render(request, "kritiken/new.html", context)


@login_required
def eidt_review(request, pk):
    instance = Review.objects.get(pk=pk)
    if request.method == "POST":
        form = ReviewForm(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            return redirect("detail_film_url", pk=pk)

    else:
        form = ReviewForm(instance=instance)
    context = {
        "form": form
    }
    return render(request, "kritiken/edit.html", context)


@login_required
def delete_review(request, pk):
    try:
        review = Review.objects.get(pk=pk)
        film = review.film
    except Review.DoesNotExist:
        raise Http404("Does not exist")
    if request.method == "POST":
        review.delete()
        return redirect("detail_film_url", pk=film.id)
    return render(request, "kritiken/delete.html")


@login_required
@require_http_methods(["GET"])
def new_watchlist_item(request, pk):
    # film_id = request.POST.get("pk")
    film = Film.objects.get(id=pk)
    user = request.user
    try:
        WatchlistItem.objects.create(
            owner = user,
            film = film,
        )
    except IntegrityError:
        pass
    return redirect(detail_film, pk=pk)
