from urllib.parse import urlparse
from django.urls import path
from filmkritiken.views import (
    films_list,
    new_film,
    edit_film,
    detail_film,
    delete_film,
    create_review,
    eidt_review,
    delete_review,
)

urlpatterns = [
    path("", films_list, name="films_list_url"),
    path("new/", new_film, name="new_film_url"),
    path("<int:pk>/update/", edit_film, name="edit_film_url"),
    path("<int:pk>", detail_film, name="detail_film_url"),
    path("<int:pk>/delete/", delete_film, name="delete_film_url"),
    path("<int:pk>/review/create/", create_review, name="create_review_url"),
    path("<int:pk>/review/edit/", eidt_review, name="eidt_review_url"),
    path("<int:pk>/review/delete/", delete_review, name="delete_review_url"),
]
